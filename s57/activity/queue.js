let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection
}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    let length = collection.length
    collection[length] = element;
    length ++
    return collection
}

function dequeue() {
    // In here you are going to remove the first element in the array



    for (i = 1; i < collection.length; i++){
        collection[i-1] = collection[i]
    }

    collection.length -= 1;

    return collection

}

function front() {
    // you will get the first element

    return collection[0]
}


function size() {
     // Number of elements
     let count = 0
     for(let i of collection){
        count ++
     }
     return count   
}

function isEmpty() {
    //it will check whether the function is empty or not

    for (let i of collection){
        return false
    }
    return true
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};