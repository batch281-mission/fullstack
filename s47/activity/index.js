const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


const updateName = () => {
	const updateFullName = (event) =>{
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
		console.log(event.target.value)
	};

	txtFirstName.addEventListener('keyup', updateFullName);
	txtLastName.addEventListener('keyup', updateFullName);
};

updateName();