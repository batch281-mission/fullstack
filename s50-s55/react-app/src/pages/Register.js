// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import {Button, Form, Row, Col} from 'react-bootstrap';
//we need to import the useState from the react
import {useState, useEffect, useContext} from 'react';

import {Link, Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal2 from 'sweetalert2';

export default function Register(){
	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	const [isNumPassed, setNumIsPassed] = useState(true);

	const [isPassed, setIsPassed] = useState(true);

	const [isDisabled, setIsDisabled] = useState(true);

	//we are going to add/create a state that will declare whether the password1 and password 2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true); 

	const { user, setUser} = useContext(UserContext);

	const navigate = useNavigate();
 
 	//when the email changes it will have a side effect that will console its value
	useEffect(()=>{
		if(email.length > 15){
			setIsPassed(false);
		}else{
			setIsPassed(true);
		}
	}, [email]);

	//this useEffect will disable or enable our sign up button
	useEffect(()=> {
		//we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button.
		if(firstName !== '' && lastName !== '' && email !== '' && mobileNumber !== '' && password1 !== '' && password2 !== '' && password1 === password2 && email.length <= 15){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2]);

	//function to simulate user registration
	function registerUser(event) {
		//prevent page reloading
		event.preventDefault();

	// 	alert('Thank you for registering!');

	// 	navigate('/login');

	// 	setFirstName('')
	// 	setLastName('')
	// 	setEmail('');
	// 	setPassword1('');
	// 	setPassword2('');
	// }

	fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email:email
		})
	})
	.then(response => response.json())
	.then(data => {
		if (data === true){
			Swal2.fire({
				title: 'Duplicate email found!',
				icon: 'error',
				text: 'Please provide different email'
			})
		} else {
			fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNumber
				})
			})
			.then(response => response.json())
			.then(data => {
				if (data === false){
					Swal2.fire({
						title: 'Registration Unsuccessful!',
						icon: 'error',
						text: 'Please try again'
					})
				} else {
					Swal2.fire({
						title: 'Registration Successful!',
						icon: 'success',
						text: 'You are now registered to Zuitt'
					})
					navigate('/login');
				}

			})

		}
	})
	}


	// check if mobileNo is 11 char
	useEffect(() => {
		if(mobileNumber.length > 11){
			setNumIsPassed(false)
		}else{
			setNumIsPassed(true)
		}
	}, [mobileNumber])

	//useEffect to validate whether the password1 is equal to password2
	useEffect(() => {
		if(password1 !== password2){
			setIsPasswordMatch(false);
		}else{
			setIsPasswordMatch(true);
		}

	}, [password1, password2]);

	return(
		user.id === null || user.id === undefined
		?

		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Register</h1>
				<Form onSubmit ={event => registerUser(event)}>
						<Form.Group className="mb-3" controlId="formFirstName">
						  <Form.Label>First Name</Form.Label>
						  <Form.Control 
						  	type="text" 
						  	placeholder="First Name" 
						  	value = {firstName}
						  	onChange = {event => setFirstName(event.target.value)}
						  	/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formLastName">
						  <Form.Label>Last Name</Form.Label>
						  <Form.Control 
						  	type="text" 
						  	placeholder="Last Name" 
						  	value = {lastName}
						  	onChange = {event => setLastName(event.target.value)}
						  	/>
						</Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="Enter email" 
				        	value = {email}
				        	onChange = {event => setEmail(event.target.value)}
				        	/>
				        <Form.Text className="text-danger" hidden = {isPassed}>
				          The email should not exceed 15 characters!
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formMobileNo">
				        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control 
				        	type="tel" 
				        	placeholder="Mobile Number" 
				        	value = {mobileNumber}
				        	onChange = {event => setMobileNumber(event.target.value)}
				        	/>
				        <Form.Text className="text-danger" hidden = {isNumPassed}>
				          Mobile number should have 11 characters.
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password" 
				        	value = {password1}
				        	onChange = {event => setPassword1(event.target.value)}
				        	/>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Retype your nominated password" 
				        	value = {password2}
				        	onChange = {event => setPassword2(event.target.value)}
				        	/>

				        <Form.Text className="text-danger" hidden = {isPasswordMatch}>
				          The passwords does not match!
				        </Form.Text>
				      </Form.Group>

				      

				      <Button variant="primary" type="submit" disabled = {isDisabled}>
				        Sign up
				      </Button>
				</Form>
			</Col>
		</Row>
		:
		<Navigate to = '*' />
		)
}